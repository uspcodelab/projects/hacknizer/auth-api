const mutations = `
  type Mutation {
    signup(name: String!, email: String!, password: String!, passwordConfirmation: String!): User!
    login(email: String!, password: String!): String!
    updateProfile(id: ID!, name: String, biography: String, avatar: String): User!
  }
`;

export default mutations;
