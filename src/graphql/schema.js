import { makeExecutableSchema } from "graphql-tools";

import Mutations from "./mutations";
import Queries from "./queries";
import resolvers from "./resolvers/";

const typeDefs = `
  type User {
    id: ID!
    name: String!
    email: String!
    password: String!
    biography: String
    avatar: String
  }

  ${ Queries }
  ${ Mutations }
`

export {
  typeDefs,
  resolvers
}
