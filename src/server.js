import Koa from "koa";
import Cors from "@koa/cors";
import { ApolloServer } from "apollo-server-koa";
import nats from "nats";
import db from "./db/models";

import { typeDefs, resolvers } from "./graphql/schema.js";

const PORT = process.env.PORT || 3000;

const nc = nats.connect({
  url: process.env.NATS_URL,
  preserveBuffers: true
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ ctx }) => ({
    ctx,
    db,
    nats: nc
  })
});

const app = new Koa();
app.use(Cors());


server.applyMiddleware({ app });

app.listen({ port: PORT }, () =>
  console.log(
    `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`
  ),
);

export default app;
