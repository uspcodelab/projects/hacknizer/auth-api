'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.UUID,
      unique: true,
      primaryKey: true
    },
    name: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    password: DataTypes.STRING,
    biography: DataTypes.TEXT,
    avatar: DataTypes.TEXT
  }, {
      underscored: true,
      indexes: [{ unique: true, fields: ['id', 'email'] }]
  });
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};